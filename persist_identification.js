const mongoose = require('mongoose');

const identificationSchema = new mongoose.Schema({
  lastname: String,
  firstname: String,
  register: String,
  picture: String
});

const Identification = mongoose.model('Identification', identificationSchema);
const persist = {};

persist.list = async function () {
  await mongoose.connect('mongodb://127.0.0.1:27017/test');
  const ids = await Identification.find();
  console.log(ids);
  return ids;
}

persist.save = async function (id) {
  await mongoose.connect('mongodb://127.0.0.1:27017/test');
  const temp = new Identification({lastname: id.lastname,
      firstname: id.firstname,
      register: id.register,
      picture: id.picture});
  await temp.save();
};

module.exports = persist;
