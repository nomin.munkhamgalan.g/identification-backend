const list = [];

const persist = {};

persist.list = async function() {
  return list;
};

persist.save = async function(id) {
  let temp = {};
  temp._id = list.length + 1;
  temp.lastname = id.lastname;
  temp.firstname = id.firstname;
  temp.register = id.register;
  list.push(temp);
};

module.exports = persist;
