const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const persist = require('./persist_local');

// for mongodb database
// const persist = require('./persist_identification');

const app = express();
const port = 3001;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors({
  origin: "*",
  allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept"]
}));
app.get('/api/list', (req, res) => {
  persist.list().then(rtn => res.send({list: rtn}))
      .catch(err => console.log(err));
});

app.post('/api/save', (req, res) => {
  console.log(req.body);
  persist.save(req.body);
  res.send(`received req. body: ${req.body}`);
});

app.listen(port, () => console.log(`Listening on port ${port}`));
